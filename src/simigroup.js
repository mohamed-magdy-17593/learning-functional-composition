// [].concat([])
// "".concat("")

const Sum = x => ({
  x,
  concat: o => Sum(o.x + x),
  inspect: () => `Sum(${x})`,
})

const All = x => ({
  x,
  concat: o => All(o.x && x),
  inspect: () => `All(${x})`,
})

const First = x => ({
  x,
  concat: o => First(x),
  inspect: () => `First(${x})`,
})

const res1 = Sum(1)
  .concat(Sum(2))
  .concat(Sum(3).concat(Sum(4)))

const res2 = All(true).concat(All(true))

const res3 = First('blah').concat(First('naah'))

console.log(res3)
