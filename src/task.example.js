const Task = require('data.task')
const fs = require('fs')

const readFile = (filename, enc = 'utf-8') =>
  new Task((rej, res) =>
    fs.readFile(
      filename,
      enc,
      (err, contents) => (err ? rej(err) : res(contents)),
    ),
  )

const writeFile = (filename, contants) =>
  new Task((rej, res) =>
    fs.writeFile(
      filename,
      contants,
      (err, success) => (err ? rej(err) : res(success)),
    ),
  )

const app = readFile('conf.json')
  .map(contents => contents.replace(/8/g, '6'))
  .chain(contants => writeFile('conf.json', contants))

app.fork(e => console.log(e), x => console.log('success'))
