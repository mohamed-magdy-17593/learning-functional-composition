const Box = x => ({
  ap: b2 => b2.map(x),
  chain: f => f(x),
  map: f => Box(f(x)),
  fold: f => f(x),
  inspect: () => `Box(${x})`,
})

const add = x => y => x + y

// ap method make it applicative

// rules
// F(x).map(f) == F(f).ap(F(x))
const liftA2 = (f, fx, fy) => fx.map(f).ap(fy)

const res = liftA2(add, Box(2), Box(3))

console.log(res)
