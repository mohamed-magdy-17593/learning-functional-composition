const add = x => y => x + y

const inc = add(1)

const modulo = dvr => dvd => dvd % dvr

const isOdd = modulo(2)

const filter = pred => xs => xs.filter(pred)

const map = f => xs => xs.map(f)

const getAllOdds = filter(isOdd)

const replace = regax => repl => str => str.replace(regax, repl)

const censor = replace(/[aeiou]/gi)('*')

const censorAll = map(censor)

const res = censor('hello world')

console.log(res)
