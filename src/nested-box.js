const Box = x => ({
  map: f => Box(f(x)),
  fold: f => f(x),
  inspect: () => `Box(${x})`,
})

const moneyToFloat = str =>
  Box(str)
    .map(s => s.replace(/\$/g, ''))
    .fold(parseFloat)

const percentToFloat = str =>
  Box(str)
    .map(s => s.replace(/\%/g, ''))
    .map(parseFloat)
    .fold(n => n * 0.01)

const applyDiscount = (price, discount) =>
  Box(moneyToFloat(price)).fold(cost =>
    Box(percentToFloat(discount)).fold(saving => cost - cost * saving),
  )

console.log(applyDiscount('$5.00', '20%'))
