const {Map} = require('immutable-ext')

const Sum = x => ({
  x,
  concat: o => Sum(o.x + x),
  inspect: () => `Sum(${x})`,
})

const All = x => ({
  x,
  concat: o => All(o.x && x),
  inspect: () => `All(${x})`,
})

const First = x => ({
  x,
  concat: o => First(x),
  inspect: () => `First(${x})`,
})

const acc1 = Map({
  name: First('Nico'),
  isPaid: All(true),
  points: Sum(10),
  friends: ['Franklin'],
})

const acc2 = Map({
  name: First('Nico'),
  isPaid: All(false),
  points: Sum(2),
  friends: ['Gatsby'],
})

const res = acc1.concat(acc2)
console.log(res.toJS())
