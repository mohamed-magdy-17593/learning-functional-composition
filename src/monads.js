// of chain Either Task List

const join = m => m.chain(x => x)

// 1 low
const m = Box(Box(Box(3)))
const res1 = join(m.map(join))
const res2 = join(join(m))

// 2 low
const m = Box('wander')
const res1 = join(Box.of(m))
const res2 = join(m.map(Box.of))

// map from monads
m.chain(x => M.of(f(x)))
