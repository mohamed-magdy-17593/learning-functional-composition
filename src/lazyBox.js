const Box = x => ({
  map: f => Box(f(x)),
  fold: f => f(x),
  inspect: () => `Box(${x})`,
})

const LazyBox = g => ({
  map: f => LazyBox(() => f(g())),
  fold: f => f(g()),
})

LazyBox(() => ' g ')
  .map(x => x.trim())
  .map(x => x.toUpperCase())
  .fold(console.log)
