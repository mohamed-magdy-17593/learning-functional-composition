const Sum = x => ({
  x,
  concat: o => Sum(o.x + x),
  inspect: () => `Sum(${x})`,
})

Sum.empty = () => Sum(0)

const sumRes = Sum.empty()
  .concat(Sum(3))
  .concat(Sum(2))
console.log(sumRes)

const All = x => ({
  x,
  concat: o => All(o.x && x),
  inspect: () => `All(${x})`,
})

All.empty = () => All(true)

const allRes = All.empty()
  .concat(All(false))
  .concat(All(true))
console.log(allRes)

// First is simigroup not a monad
const First = x => ({
  x,
  concat: o => First(x),
  inspect: () => `First(${x})`,
})
