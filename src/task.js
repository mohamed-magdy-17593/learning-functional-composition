const Task = require('data.task')

const launchMissiles = () =>
  new Task((rej, res) => {
    console.log('launchMissiles')
    res('missiles')
  })

const app = launchMissiles().map(x => x + '!')

app.fork(e => console.log('err', e), x => console.log('success', x))
